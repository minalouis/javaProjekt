SELECT --i.subdivision_id,
      -- i.amount,
       --d.id_division,
       d.division_name,
       SUM(i.amount) 
  FROM item i
       LEFT JOIN
       subdivision sd ON i.subdivision_id = sd.id_subdivision
       LEFT JOIN
       division d ON d.id_division = sd.division_id
 WHERE i.income_id = 0
 GROUP BY d.id_division;
