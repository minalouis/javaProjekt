SELECT  s.shop_name,
        SUM(i.amount) AS Summe, i.date

    FROM item i
        LEFT JOIN 
        shop s ON i.shop_id = s.id_shop
    WHERE 
        i.shop_id != 0
        AND strftime('%m',i.date) BETWEEN (strftime('%m','now') - 2) AND strftime('%m','now')
      
    GROUP BY s.shop_name, strftime('%m',i.date) 
    ORDER BY strftime('%m',i.date), Summe DESC
 ;