SELECT s.shop_name, SUM(i.amount) AS Summe,strftime('%m',i.date) AS month 
    FROM item i
        LEFT JOIN 
        shop s ON i.shop_id = s.id_shop
    WHERE 
        --s.shop_name = 'Rewe' 
        i.shop_id != 0 AND
         strftime('%m',i.date) BETWEEN (strftime('%m','now') - 2) AND strftime('%m','now')     
    GROUP BY s.shop_name, strftime('%m',i.date) 
    ORDER BY strftime('%m',i.date), Summe DESC
 ;
 
SELECT s.shop_name,SUM(i.amount) AS Summe,strftime('%m',i.date) AS month
    FROM item i
        LEFT JOIN 
        shop s ON i.shop_id = s.id_shop
    WHERE 
        i.shop_id != 0 AND
         strftime('%Y%m',i.date) = '201802'      
    GROUP BY s.shop_name, strftime('%m',i.date)  
    LIMIT 3
 ;