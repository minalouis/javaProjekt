--Summen aller gefüllten Unterkategorien, ohne Einkommen
SELECT d.division_name, s.subdivision_name, SUM(i.amount) AS sum
FROM  item i
    LEFT JOIN subdivision s 
        ON i.subdivision_id = s.id_subdivision      
    LEFT JOIN division d
        ON d.id_division = s.division_id 
WHERE i.subdivision_id != 0
    AND d.id_division = 1
--WHERE d.id_division != 0 
GROUP BY s.id_subdivision;

--alle Kategorien (ohne sonstiges)
SELECT id_division, division_name FROM division
WHERE id_division != 0;

-- alle Unterkategorien, auch ohne Einträge, ohne Einkommen
   SELECT d.division_name, s.subdivision_name, SUM(i.amount) AS sum, strftime('%m', date) as month, strftime('%Y', date) AS year
    FROM subdivision s 
        LEFT JOIN  item i
            ON i.subdivision_id = s.id_subdivision
        LEFT JOIN division d
            ON d.id_division = s.division_id  
    WHERE d.id_division != 0 AND d.id_division = 1
    GROUP BY s.id_subdivision, strftime('%Y-%m', date)
    ORDER BY  division_name, year, month
;

-- Auswertungstabelle mit Monaten als Spalten
SELECT year, subdivision_name
     , SUM(CASE WHEN strftime('%m', date) = '01' THEN sum END) Jan
     , SUM(CASE WHEN strftime('%m', date) = '02' THEN sum END) Feb
     , SUM(CASE WHEN strftime('%m', date) = '03' THEN sum END) Mrz
     , SUM(CASE WHEN strftime('%m', date) = '04' THEN sum END) Apr
     , SUM(CASE WHEN strftime('%m', date) = '05' THEN sum END) Mai
     , SUM(CASE WHEN strftime('%m', date) = '06' THEN sum END) Jun
     , SUM(CASE WHEN strftime('%m', date) = '07' THEN sum END) Jul
     , SUM(CASE WHEN strftime('%m', date) = '08' THEN sum END) Aug
     , SUM(CASE WHEN strftime('%m', date) = '09' THEN sum END) Sep
     , SUM(CASE WHEN strftime('%m', date) = '10' THEN sum END) Okt
     , SUM(CASE WHEN strftime('%m', date) = '11' THEN sum END) Nov
     , SUM(CASE WHEN strftime('%m', date) = '12' THEN sum END) Dez
     
FROM(
    SELECT i.date,d.division_name, s.subdivision_name, SUM(i.amount) AS sum, strftime('%Y', date) AS year
    FROM subdivision s 
        LEFT JOIN  item i
            ON i.subdivision_id = s.id_subdivision
        LEFT JOIN division d
            ON d.id_division = s.division_id  
    WHERE d.id_division = 1 AND (year IS NULL OR year LIKE '2018')
    GROUP BY s.id_subdivision, strftime('%Y-%m', date)
    ORDER BY  division_name, year
    )
    GROUP BY year, subdivision_name
;


-- was ist in subdivision 0: Einkommen
SELECT * FROM item 
WHERE subdivision_id = 0;

-- Einkommen
SELECT item_description, income_kind, amount, date FROM item
    LEFT JOIN income 
        ON income.id_income = item.income_id
        WHERE id_income != 0;
        
-- Abfrage für TableView
SELECT item_description, date, amount 
FROM item 
WHERE income_id = 0
ORDER BY date DESC;

-- Einkommen pro Monat, Summe
SELECT SUM(amount) AS Einkommen, strftime('%m-%Y', date) AS month 
FROM item
WHERE income_id != 0
GROUP BY month;

--Gesamtausgaben pro Monat im aktuellen Jahr
SELECT SUM(amount) AS sum, strftime('%m-%Y', date) AS month 
FROM item
WHERE income_id = 0 
    AND strftime('%Y', date) = strftime('%Y', 'now') --auskommentieren für alle Ausgaben
GROUP by month;

-- Einkommen im Jahr, Summe
SELECT SUM(amount) AS Einkommen FROM item
WHERE income_id != 0 AND strftime('%Y', date) = strftime('%Y', 'now')
;

--Gesamtausgaben aktuellen Jahr
SELECT SUM(amount) AS Ausgaben FROM item
WHERE income_id = 0 AND strftime('%Y', date) = strftime('%Y', 'now')
;