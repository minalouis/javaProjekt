package helper;

import java.time.YearMonth;

public class Quarter {
	//minus 3 wenn der laufende Monat nicht im BarChart sein soll, 
	//minus 2, wenn er dabei sein soll
	private YearMonth yearMonth = YearMonth.now().minusMonths(2);
	private String[] last3months = new String[3];

	public String[] getLast3Months() {
		for(int i = 0; i < 3; i++) {
			last3months[i] = yearMonth.plusMonths(i).toString();
		}

		return last3months;
	}
}
