package evaluationDao;

import java.util.ArrayList;

public interface YearSubdivMonthsDAO {
	
	//Abfrageergebnis in Liste von YearSubdivMonths-Items speichern
	ArrayList<YearSubdivMonths> getResultList(String sql);
	
}
