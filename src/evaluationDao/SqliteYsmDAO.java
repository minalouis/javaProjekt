package evaluationDao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import dao.DatabaseConnect;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Modality;


public class SqliteYsmDAO implements YearSubdivMonthsDAO{
	private Connection conn;
	
	public SqliteYsmDAO() {
		this.conn = DatabaseConnect.getConnection();		
	}

	@Override
	public ArrayList<YearSubdivMonths> getResultList(String sql) {
		Statement statement;
		ResultSet resultSet;
		
		ArrayList<YearSubdivMonths> list = new ArrayList<>(); 
		YearSubdivMonths ysm;
		
		try {
			statement = conn.createStatement();
			resultSet = statement.executeQuery(sql);
			
			while(resultSet.next()) {
				ysm = new YearSubdivMonths();
				
				//ysm.setYear(resultSet.getString("year"));
				ysm.setSubdivision_name(resultSet.getString("subdivision_name"));			
				ysm.setJan(resultSet.getDouble("Jan"));			
				ysm.setFeb(resultSet.getDouble("Feb"));
				ysm.setMrz(resultSet.getDouble("Mrz"));			
				ysm.setApr(resultSet.getDouble("Apr"));
				ysm.setMai(resultSet.getDouble("Mai"));			
				ysm.setJun(resultSet.getDouble("Jun"));
				ysm.setJul(resultSet.getDouble("Jul"));			
				ysm.setAug(resultSet.getDouble("Aug"));
				ysm.setSep(resultSet.getDouble("Sep"));			
				ysm.setOkt(resultSet.getDouble("Okt"));
				ysm.setNov(resultSet.getDouble("Nov"));			
				ysm.setDez(resultSet.getDouble("Dez"));

				list.add(ysm);
			}
			//System.out.println(list);
			
		} catch (SQLException e) {
			Alert alert = new Alert(AlertType.ERROR);			
			alert.setTitle("Fehler");
			alert.setHeaderText("Methode YSM: getResultList(String sql)");
			alert.setContentText("SQL Statement ist fehlerhaft");
			alert.initModality(Modality.APPLICATION_MODAL);
			alert.showAndWait();
		}		
		
		return list;
	}

}
