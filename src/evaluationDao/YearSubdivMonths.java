package evaluationDao;

public class YearSubdivMonths {
	private String year;
	private String subdivision_name;
	private double jan;
	private double feb;
	private double mrz;
	private double apr;
	private double mai;
	private double jun;
	private double jul;
	private double aug;
	private double sep;
	private double okt;
	private double nov;
	private double dez;
	private double sum;
	
	public double getSubdivsionSum() {
		return jan+feb+mrz+apr+mai+jun+jul+aug+sep+okt+nov+dez;
	}
	
	public double getSum() {
		return sum;
	}

	public void setSum(double sum) {
		this.sum = getSubdivsionSum();
	}

	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getSubdivision_name() {
		return subdivision_name;
	}
	public void setSubdivision_name(String subdivision_name) {
		this.subdivision_name = subdivision_name;
	}
	public double getJan() {
		return jan;
	}
	public void setJan(double jan) {
		this.jan = jan;
	}
	public double getFeb() {
		return feb;
	}
	public void setFeb(double feb) {
		this.feb = feb;
	}
	public double getMrz() {
		return mrz;
	}
	public void setMrz(double mrz) {
		this.mrz = mrz;
	}
	public double getApr() {
		return apr;
	}
	public void setApr(double apr) {
		this.apr = apr;
	}
	public double getMai() {
		return mai;
	}
	public void setMai(double mai) {
		this.mai = mai;
	}
	public double getJun() {
		return jun;
	}
	public void setJun(double jun) {
		this.jun = jun;
	}
	public double getJul() {
		return jul;
	}
	public void setJul(double jul) {
		this.jul = jul;
	}
	public double getAug() {
		return aug;
	}
	public void setAug(double aug) {
		this.aug = aug;
	}
	public double getSep() {
		return sep;
	}
	public void setSep(double sep) {
		this.sep = sep;
	}
	public double getOkt() {
		return okt;
	}
	public void setOkt(double okt) {
		this.okt = okt;
	}
	public double getNov() {
		return nov;
	}
	public void setNov(double nov) {
		this.nov = nov;
	}
	public double getDez() {
		return dez;
	}
	public void setDez(double dez) {
		this.dez = dez;
	}
	
	

}
