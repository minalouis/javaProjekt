package dao;

import java.util.ArrayList;

public class ItemList {
	private ArrayList<Item> itemList = new ArrayList<>();
	
	public boolean addItem(Item i) {
		if(i != null)
			return itemList.add(i);
		else
			return false;
	}
	
	public int getNumberOfItems() {
		return itemList.size();
	}
	

	public void printItem() {
		for(int i = 0; i < itemList.size(); i++) {
			System.out.println(itemList.get(i));
		}
	}

	public Item getItem(int i) {
		if(isValidIndex(i))
			return itemList.get(i);
		else 
			return null;
	}

	public void deleteItem(int i) {
		if(isValidIndex(i))
			itemList.remove(i);
	}

	public void clearList() {
		itemList.clear();		
	}
	
	private boolean isValidIndex(int i){
		return (i >= 00 && i < itemList.size());
	}

}
