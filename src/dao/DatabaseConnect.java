package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnect {
	private static Connection conn = null;
	private static String  connectionString = "jdbc:sqlite:db/housekeepingBook.db";
		
	private DatabaseConnect() {		
	}
	
	public static Connection getConnection() {
		if(conn == null) {
			try {
				conn = DriverManager.getConnection(connectionString);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return conn;		
	}
}
