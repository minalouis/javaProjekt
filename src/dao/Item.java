package dao;
/**
 * Item: ein Eintrag in der Datenbank
 * @author phamann
 * 06.03.2018
 */
public class Item {
	private int idItem;
	private String itemDescription;
	private String date;
	private double amount;
	private int subdivisionId;
	private String shop;
	private int shop_id;
	private int incomeId;
	
	public int getIdItem() {
		return idItem;
	}
	
	// set idItem macht die Datenbank

	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	public String getDate() {
		return date;
	}	
	public void setDate(String date) {
		this.date = date;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	public int getSubdivisionId() {
		return subdivisionId;
	}
	public void setSubdivisionId(int subdivisionId) {
		this.subdivisionId = subdivisionId;
	}
	public String getShop() {
		return shop;
	}
	public void setShop(String shop) {
		this.shop = shop;
	}	
	public int getShop_id() {
		return shop_id;
	}
	public void setShop_id(int shop_id) {
		this.shop_id = shop_id;
	}
	public int getIncomeId() {
		return incomeId;
	}
	public void setIncomeId(int incomeId) {
		this.incomeId = incomeId;
	}

	@Override
	public String toString() {
		return "\nDatum: " + this.date + "\nBetrag: " + this.amount + "\nArtikel: " + this.itemDescription
				+ "\nUK_ID: " + this.subdivisionId + "\nShopID: " + this.shop_id + "\n";
	}

}
