package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import dashboardElements.BarChartData;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart.Data;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Modality;
import javafx.scene.chart.XYChart;

public class SqliteItemDAO implements ItemDAO{
	private Connection conn;
	
	public SqliteItemDAO() {
		this.conn = DatabaseConnect.getConnection();		
	}

	@Override
	public void insertItem(Item item){

		String sql = "INSERT INTO item (item_description, date, amount, subdivision_id, shop, shop_id, income_id) VALUES (?,?,?,?,?,?,?)";
		try {
			PreparedStatement prepState = conn.prepareStatement(sql);
			prepState.setString(1, item.getItemDescription());
			prepState.setString(2, item.getDate());
			prepState.setDouble(3, item.getAmount());
			prepState.setInt(4,item.getSubdivisionId());
			prepState.setString(5, item.getShop());
			prepState.setInt(6,item.getShop_id());
			prepState.setInt(7,item.getIncomeId());	
			
			prepState.executeUpdate();
			
		} catch (SQLException e) {
			Alert alert = new Alert(AlertType.ERROR);			
			alert.setTitle("Fehler");
			alert.setHeaderText("Methode: insertItem(Item item)");
			alert.setContentText("SQL Statement ist fehlerhaft");
			alert.initModality(Modality.APPLICATION_MODAL);
			alert.showAndWait();
		}
		
	}

	@Override
	public ArrayList<Item> getResultList(String sql) {
		
		Statement statement;
		ResultSet resultSet;
		ArrayList<Item> list = new ArrayList<>(); 
		Item item;
		
		try {
			statement = conn.createStatement();
			resultSet = statement.executeQuery(sql);
			
			while(resultSet.next()) {

				item = new Item();
				item.setDate(resultSet.getString("date"));			
				item.setAmount(resultSet.getDouble("amount"));
				item.setItemDescription(resultSet.getString("item_description"));
				//Für die TableView braucht man nur die drei vorherigen Daten aus dem ResultSet
//				item.setSubdivisionId(resultSet.getInt("subdivision_id"));
//				item.setShop(resultSet.getString("shop"));
//				item.setShop_id(resultSet.getInt("shop_id"));
//				item.setIncomeId(resultSet.getInt("income_id"));

				list.add(item);
			}
			//System.out.println(list);
			
		} catch (SQLException e) {
			Alert alert = new Alert(AlertType.ERROR);			
			alert.setTitle("Fehler");
			alert.setHeaderText("Methode: getResultList(String sql)");
			alert.setContentText("SQL Statement ist fehlerhaft");
			alert.initModality(Modality.APPLICATION_MODAL);
			alert.showAndWait();
		}		
		
		return list;
	}

	@Override
	public ObservableList<Data> getSumsOfDivisions(String sql) { 
		
		Statement statement;
		ResultSet resultSet;
		ObservableList<Data> list = FXCollections.observableArrayList();
		
		try {
			statement = conn.createStatement();
			resultSet = statement.executeQuery(sql);
			
			while(resultSet.next()) {				
				 list.add(new Data(resultSet.getString(1), resultSet.getDouble(2)));				
			}
		}	
		
		catch (SQLException e) {
			Alert alert = new Alert(AlertType.ERROR);			
			alert.setTitle("Fehler");
			alert.setHeaderText("Methode: getSumsOfDivisions(String sql)");
			alert.setContentText("SQL Statement ist fehlerhaft");
			alert.initModality(Modality.APPLICATION_MODAL);
			alert.showAndWait();
		}		
	
		return list;
	}
	
	@Override
	public XYChart.Series<String, Number> getDataSerieForBarChart(String shop) { 
		String sql = "SELECT  s.shop_name, SUM(i.amount) AS Summe,strftime('%m',i.date) AS month" + 
					 " FROM item i" + 
					 " LEFT JOIN" + 
					 " shop s ON i.shop_id = s.id_shop" + 
					 " WHERE" + 
					 " s.shop_name = '" + shop + "'" + 
					 " AND strftime('%m',i.date) BETWEEN (strftime('%m','now') - 2) AND strftime('%m','now')" + 
					 " GROUP BY s.shop_name, strftime('%m',i.date)" + 
					 " ORDER BY strftime('%m',i.date);";
		
		Statement statement;
		ResultSet resultSet; 
		XYChart.Series<String, Number> serie =  new XYChart.Series<>();
		
		try {
			statement = conn.createStatement();
			resultSet = statement.executeQuery(sql);
			
			while(resultSet.next()) {	
				serie.getData().add(new XYChart.Data<>(resultSet.getString(3), resultSet.getDouble(2)));				
			}
		}	
		
		catch (SQLException e) {
			Alert alert = new Alert(AlertType.ERROR);			
			alert.setTitle("Fehler");
			alert.setHeaderText("Methode: getDataSerieForBarChart(String shop)");
			alert.setContentText("SQL Statement ist fehlerhaft");
			alert.initModality(Modality.APPLICATION_MODAL);
			alert.showAndWait();
		}		
	
		return serie;
	}
	
	@Override
	public HashMap<String,ArrayList<BarChartData>> barChartDataMap(String... yearMonth) {
		HashMap<String,ArrayList<BarChartData>> map = new HashMap<>();
        String connectionString = "jdbc:sqlite:db/housekeepingBook.db";

        try (Connection conn = DriverManager.getConnection(connectionString)) {
            String sql;

            for (int i = 0; i < yearMonth.length; i++) {
                sql = "SELECT s.shop_name, SUM(i.amount) AS summe, strftime('%Y-%m',i.date) AS month"
                        + " FROM item i"
                        + " LEFT JOIN shop s ON i.shop_id = s.id_shop"
                        + " WHERE i.shop_id != 0 AND "
                        + " strftime('%Y-%m',i.date) = '" + yearMonth[i] + "'"
                       // + "strftime('%m',i.date) ='"+ month[i] +"'"
                        + " GROUP BY s.shop_name, strftime('%m',i.date)"
                        + " ORDER BY strftime('%m',i.date), Summe DESC" 
                        + " LIMIT 3"; 
                
                Statement statement = conn.createStatement();
                ResultSet res = statement.executeQuery(sql);
                
                while (res.next()) {
                    BarChartData b = new BarChartData();
                    b.setShopname(res.getString("shop_name"));                    
                    if(!map.containsKey(b.getShopname()))
                        map.put(b.getShopname(), new ArrayList<BarChartData>());
                    b.setSum(res.getDouble("summe"));
                    b.setyearMonth(res.getString("month"));
                    map.get(b.getShopname()).add(b);
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
		return map;

    }
	
	@Override
	public ArrayList<String> getDistinctShops() {
		ArrayList<String> shops = new ArrayList<>();

		String sql = "SELECT DISTINCT shop_name" + 
					 " FROM (" + 
					 "   SELECT  s.shop_name," + 
					 "   SUM(i.amount) AS Summe, i.date" + 
					 "   FROM item i" + 
					 "   LEFT JOIN shop s ON i.shop_id = s.id_shop" + 
					 "   WHERE i.shop_id != 0" + 
					 "   AND strftime('%m',i.date) BETWEEN (strftime('%m','now') - 2) AND strftime('%m','now')" + 
					 "   GROUP BY s.shop_name , strftime('%m',i.date) " + 
					 "   ORDER BY Summe DESC" + 
					 " )";
		Statement statement;
		ResultSet resultSet;
		
		try {
			statement = conn.createStatement();
			resultSet = statement.executeQuery(sql);
			
			while(resultSet.next()) {	
				int i = 0;
				shops.add(resultSet.getString(i +1));
				i++;
			}
		}	
		
		catch (SQLException e) {
			Alert alert = new Alert(AlertType.ERROR);			
			alert.setTitle("Fehler");
			alert.setHeaderText("Methode: getDistinctShops()");
			alert.setContentText("SQL Statement ist fehlerhaft");
			alert.initModality(Modality.APPLICATION_MODAL);
			alert.showAndWait();
		}			
		return shops;
	}
	
	@Override
	public ObservableList<Category> getComboBoxItems(String sql){
		Category cat;
		Statement statement;
		ResultSet resultSet;
		ObservableList<Category> comboBoxItems = FXCollections.observableArrayList();
		
		try {
			statement = conn.createStatement();
			resultSet = statement.executeQuery(sql);
			
			while(resultSet.next()) {	
				cat = new Category();
				cat.setId(resultSet.getInt(1));
				cat.setName(resultSet.getString(2));
				comboBoxItems.add(cat);					
			}
		}	
		
		catch (SQLException e) {
			Alert alert = new Alert(AlertType.ERROR);			
			alert.setTitle("Fehler");
			alert.setHeaderText("Methode: getComboBoxItems(String sql)");
			alert.setContentText("SQL Statement ist fehlerhaft");
			alert.initModality(Modality.APPLICATION_MODAL);
			alert.showAndWait();
		}		
	
		return comboBoxItems;
	}

	
	@Override
	public double getSingleResult(String sql) {
		Statement statement;
		ResultSet resultSet;
		double result = 0.0;
		
		try {
			statement = conn.createStatement();
			resultSet = statement.executeQuery(sql);
			
			result = resultSet.getDouble(1);
		}	
		
		catch (SQLException e) {
			Alert alert = new Alert(AlertType.ERROR);			
			alert.setTitle("Fehler");
			alert.setHeaderText("Methode: getSingleResult(String sql)");
			alert.setContentText("SQL Statement ist fehlerhaft");
			alert.initModality(Modality.APPLICATION_MODAL);
			alert.showAndWait();
		}		
		
		return result;
	}
	
}
