package dao;

import java.util.ArrayList;
import java.util.HashMap;

import dashboardElements.BarChartData;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.PieChart.Data;
/**
 * Interface Data (Item) Access Object
 * @author phamann
 * 06.03.2018
 */

public interface ItemDAO {
// Eintrag in die Datenbank
	void insertItem(Item item);
	
// Abfrageergebnis in einer Liste von Items speichern
	ArrayList<Item> getResultList(String sql);
	
// Die Summen der Hauptkategorien speichern in eine ObservableList (für PieChart)
	ObservableList<Data> getSumsOfDivisions(String sql);	
	
// Chartserien für BarChart erstellen
	XYChart.Series<String, Number> getDataSerieForBarChart(String shop);
	
// 	ArrayList mit vorgekommenen Läden
	/*
	 * diese Funktion wird momentan nicht mehr gebraucht nach dem Umbau auf Top3 Monatsabfrage
	 * liefert die Läden zurück, die zu den TOP3 der letzten Monate (muss noch auf Quarter umgebaut werden)
	 * gehören
	 */
	ArrayList<String> getDistinctShops();
	
// OL von Category-Objekten, die ID und Kategorie speichern	
	ObservableList<Category> getComboBoxItems(String sql);	
	
// HashMap enthält Daten für BarChart FX, String: key (Laden), ArrayList mit BarChartData:
// enthält Ergebniszeilen aus DB Abfrage passend zum Laden
	HashMap<String,ArrayList<BarChartData>> barChartDataMap(String... yearMonth);
	
// Einzelnen Wert abfragen
	double getSingleResult(String sql);

//TODO
	//updateItem(Item item);
	
	//void deleteItem(Item item);
	
	//void selectItem(Item item);
	
}


