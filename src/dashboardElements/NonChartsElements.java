package dashboardElements;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import dao.Category;
import dao.Item;
import dao.SqliteItemDAO;
import evaluationDao.SqliteYsmDAO;
import evaluationDao.YearSubdivMonths;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.skin.DatePickerSkin;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

public class NonChartsElements {
	static SqliteItemDAO dao = new SqliteItemDAO();
	static SqliteYsmDAO ysmDao = new SqliteYsmDAO();
	static BorderPane borderPane; 
	 
	public static GridPane getNaviBoxContent() {
		
		//Auswahl Actions  
    	Label headline = new Label("Was soll erledigt werden?");
    	Button newEntryButton = new Button("Ausgabe eintragen");
    	newEntryButton.setPrefWidth(120);
    	Button changeButton = new Button("Daten ändern");
    	changeButton.setPrefWidth(120);
    	Button evaluationButton = new Button("Auswertungen");
    	evaluationButton.setPrefWidth(120);
    	
        GridPane entryBox = new GridPane();
        entryBox.setGridLinesVisible(false);
        
        entryBox.setPrefWidth(220);
        entryBox.setVgap(10);
        entryBox.setHgap(10);
        
        entryBox.add(headline, 0, 0, 2, 1);
        
        entryBox.add(newEntryButton, 0, 2);
        entryBox.add(changeButton, 0, 3);
        entryBox.add(evaluationButton, 0, 4); 
        entryBox.setMaxWidth(150);
		
		return entryBox;		
	}
	
	public static Node getPopUpContent() {
	
		DatePicker datePicker = new DatePicker(LocalDate.now());
	    DatePickerSkin datePickerSkin = new DatePickerSkin(datePicker);
	    Node popupContent = datePickerSkin.getPopupContent();
	    
	    //aktualisieren des gewählten Datums
	    datePicker.valueProperty().addListener((observable, oldValue, newValue) -> {System.out.println(newValue);});
	    
		return popupContent;
	}
	
	public static TableView<Item> getTableView(){
		
        ArrayList<Item> list = new ArrayList<>();
        //String sql = "SELECT * FROM item ORDER BY date DESC";
        String sql = "SELECT item_description, date, amount FROM item" + 
        		" WHERE income_id = 0" + 
        		" ORDER BY date DESC";
		list = dao.getResultList(sql);		 
		
        TableView<Item> table = new TableView<Item>();	        
        
        // Daten in Collection ablegen
        ObservableList<Item> data = FXCollections.observableArrayList(list);
        table.setItems(data);
        
        // Spalten erzeugen und verknüpfen
        TableColumn<Item,String> dateColumn = new TableColumn<>("Datum");
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        
        //Anzeige des Datums im Format TT.MM.JJJJ
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        dateColumn.setCellFactory(column -> new TableCell<Item, String>() {
            @Override
            protected void updateItem(String date, boolean empty) {
                super.updateItem(date, empty);
                if (date == null || empty) {
                    setText(null);
                    setStyle("");
                }
                else {
                    setText(dateFormatter.format(LocalDate.parse(date)));
                }
            }
        });
        
        table.getColumns().add(dateColumn);
        
        TableColumn<Item,Double> amountColumn = new TableColumn<>("Betrag");
        amountColumn.setCellValueFactory(new PropertyValueFactory<>("amount"));
        
        //zum Sortieren der Preise:
        amountColumn.setComparator((price1, price2) -> price1.compareTo(price2));
        
        table.getColumns().add(amountColumn);
        
        TableColumn<Item,String> descriptionColumn = new TableColumn<>("Beschreibung");
        descriptionColumn.setCellValueFactory(new PropertyValueFactory<>("itemDescription"));
        descriptionColumn.setPrefWidth(250);
        table.getColumns().add(descriptionColumn);
        	        
        table.setPrefWidth(410);
        table.setMinWidth(200);
        
		return table;
	}
	
public static TableView<YearSubdivMonths> getTableViewEvaluation(Category category){
	System.out.println(category);
		
        ArrayList<YearSubdivMonths> list = new ArrayList<>();
        String sql = "SELECT year, subdivision_name\r\n" + 
        		"     , SUM(CASE WHEN strftime('%m', date) = '01' THEN sum END) Jan\r\n" + 
        		"     , SUM(CASE WHEN strftime('%m', date) = '02' THEN sum END) Feb\r\n" + 
        		"     , SUM(CASE WHEN strftime('%m', date) = '03' THEN sum END) Mrz\r\n" + 
        		"     , SUM(CASE WHEN strftime('%m', date) = '04' THEN sum END) Apr\r\n" + 
        		"     , SUM(CASE WHEN strftime('%m', date) = '05' THEN sum END) Mai\r\n" + 
        		"     , SUM(CASE WHEN strftime('%m', date) = '06' THEN sum END) Jun\r\n" + 
        		"     , SUM(CASE WHEN strftime('%m', date) = '07' THEN sum END) Jul\r\n" + 
        		"     , SUM(CASE WHEN strftime('%m', date) = '08' THEN sum END) Aug\r\n" + 
        		"     , SUM(CASE WHEN strftime('%m', date) = '09' THEN sum END) Sep\r\n" + 
        		"     , SUM(CASE WHEN strftime('%m', date) = '10' THEN sum END) Okt\r\n" + 
        		"     , SUM(CASE WHEN strftime('%m', date) = '11' THEN sum END) Nov\r\n" + 
        		"     , SUM(CASE WHEN strftime('%m', date) = '12' THEN sum END) Dez\r\n" + 
        		"     \r\n" + 
        		" FROM(\r\n" + 
        		"    SELECT i.date,d.division_name, s.subdivision_name, SUM(i.amount) AS sum, strftime('%Y', date) AS year\r\n" + 
        		"    FROM subdivision s \r\n" + 
        		"        LEFT JOIN  item i\r\n" + 
        		"            ON i.subdivision_id = s.id_subdivision\r\n" + 
        		"        LEFT JOIN division d\r\n" + 
        		"            ON d.id_division = s.division_id  \r\n" + 
        		//"    WHERE d.id_division = 1 AND (year IS NULL OR year LIKE '2018')\r\n" + 
        		"    WHERE d.division_name = '" + category + "' AND (year IS NULL OR year LIKE '2018')\r\n" + 
        		"    GROUP BY s.id_subdivision, strftime('%Y-%m', date)\r\n" + 
        		"    ORDER BY  division_name, year\r\n" + 
        		"    )\r\n" + 
        		"    GROUP BY year, subdivision_name\r\n" + 
        		";\r\n";
        
		list = ysmDao.getResultList(sql);
		
        TableView<YearSubdivMonths> table = new TableView<YearSubdivMonths>();	        
        
        // Daten in Collection ablegen
        ObservableList<YearSubdivMonths> data = FXCollections.observableArrayList(list);
        table.setItems(data);
        
        // Spalten erzeugen und verknüpfen          
        TableColumn<YearSubdivMonths, String> subDivColumn = new TableColumn<>("Kategorie");
        subDivColumn.setCellValueFactory(new PropertyValueFactory<>("subdivision_name"));
        table.getColumns().add(subDivColumn);
        
        TableColumn<YearSubdivMonths, String> janColumn = new TableColumn<>("Jan");
        janColumn.setCellValueFactory(new PropertyValueFactory<>("Jan"));
        table.getColumns().add(janColumn);
        
        TableColumn<YearSubdivMonths, String> febColumn = new TableColumn<>("Feb");
        febColumn.setCellValueFactory(new PropertyValueFactory<>("Feb"));
        table.getColumns().add(febColumn);
        
        TableColumn<YearSubdivMonths, String> mrzColumn = new TableColumn<>("Mrz");
        mrzColumn.setCellValueFactory(new PropertyValueFactory<>("Mrz"));
        table.getColumns().add(mrzColumn);
        
        TableColumn<YearSubdivMonths, String> aprColumn = new TableColumn<>("Apr");
        aprColumn.setCellValueFactory(new PropertyValueFactory<>("Apr"));
        table.getColumns().add(aprColumn);
        
        TableColumn<YearSubdivMonths, String> maiColumn = new TableColumn<>("Mai");
        maiColumn.setCellValueFactory(new PropertyValueFactory<>("Mai"));
        table.getColumns().add(maiColumn);
        
        TableColumn<YearSubdivMonths, String> junColumn = new TableColumn<>("Jun");
        junColumn.setCellValueFactory(new PropertyValueFactory<>("Jun"));
        table.getColumns().add(junColumn);
        
        TableColumn<YearSubdivMonths, String> julColumn = new TableColumn<>("Jul");
        julColumn.setCellValueFactory(new PropertyValueFactory<>("Jul"));
        table.getColumns().add(julColumn);
        
        TableColumn<YearSubdivMonths, String> augColumn = new TableColumn<>("Aug");
        augColumn.setCellValueFactory(new PropertyValueFactory<>("Aug"));
        table.getColumns().add(augColumn);
        
        TableColumn<YearSubdivMonths, String> sepColumn = new TableColumn<>("Sep");
        sepColumn.setCellValueFactory(new PropertyValueFactory<>("Sep"));
        table.getColumns().add(sepColumn);
        
        TableColumn<YearSubdivMonths, String> oktColumn = new TableColumn<>("Okt");
        oktColumn.setCellValueFactory(new PropertyValueFactory<>("Okt"));
        table.getColumns().add(oktColumn);
        
        TableColumn<YearSubdivMonths, String> novColumn = new TableColumn<>("Nov");
        novColumn.setCellValueFactory(new PropertyValueFactory<>("Nov"));
        table.getColumns().add(novColumn);
        
        TableColumn<YearSubdivMonths, String> dezColumn = new TableColumn<>("Dez");
        dezColumn.setCellValueFactory(new PropertyValueFactory<>("Dez"));
        table.getColumns().add(dezColumn);
        
        //zum Sortieren der Preise:
        //amountColumn.setComparator((price1, price2) -> price1.compareTo(price2));
        table.setPrefWidth(650);
        table.setPrefHeight(300);
        
		return table;
	}
	
}
