package dashboardElements;

import java.util.ArrayList;
import java.util.HashMap;
import dao.SqliteItemDAO;
import helper.Quarter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.PieChart.Data;

/**
 * In dieser Klasse werden die verschiedenen Charts Elemente erzeugt, die im Dashboard
 * in die einzelnen Container eingebaut werden
 * 
 * @author phamann
 * 02.03.2018
 */
public class ChartsForDashboard{
	
	static SqliteItemDAO dao = new SqliteItemDAO(); 
	static Quarter quarter = new Quarter();
	
	public static BarChart<String, Number> getBarChartDashboard() {
		
		HashMap<String,ArrayList<BarChartData>> map = new HashMap<>();
	   	String[] last3months = quarter.getLast3Months();
		
        map = dao.barChartDataMap(last3months);

        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("Monat");
        xAxis.setCategories(FXCollections.observableArrayList(last3months));
		
        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Euro");

        // Create a BarChart
        BarChart<String, Number> barChart = new BarChart<>(xAxis, yAxis);

        for(String key: map.keySet()) {
            XYChart.Series<String, Number> dataSerie = new XYChart.Series<>();
            dataSerie.setName(key);
            
            for(int i=0; i<map.get(key).size();i++) {
                BarChartData b = map.get(key).get(i);
                dataSerie.getData().add(new XYChart.Data<String, Number>(b.getyearMonth(), b.getSum()));
            }
            
            barChart.getData().add(dataSerie);
        }

		barChart.setTitle("Die Top-3-Lebensmittelläden");
		barChart.setPrefWidth(450);
		
		return barChart;
	}

	public static PieChart getPieChartDashboard() {
			
// Preparing ObservableList object           
        ObservableList<Data> sums;
        String sql = "SELECT d.division_name, SUM(i.amount)" + 
				 	 " FROM item i" + 
				 	 " LEFT JOIN" + 
				 	 " subdivision sd ON i.subdivision_id = sd.id_subdivision" + 
				 	 " LEFT JOIN" + 
				 	 " division d ON d.id_division = sd.division_id" + 
				 	 " WHERE i.income_id = 0 " + 
				 	 " GROUP BY d.id_division;"
			;
        sums = dao.getSumsOfDivisions(sql);    

// Creating a Pie chart 
        PieChart pieChart = new PieChart(sums);
        pieChart.setTitle("Anteile an Gesamtausgaben");
        
        pieChart.setMinWidth(200);
        pieChart.setPrefWidth(410);
        pieChart.setPrefHeight(280);	 
        
		return pieChart;			
	}
	
//	public static BarChart<String, Number> getBarChartDashboard() {
//		/**
//		 * BarChart hat zwei Achsen:
//		 * 		X-Achse mit Strings beschriftet
//		 * 		Y-Achse mit Zahlen
//		 * <Series<String, Number>>
//		 * Series enthalten
//		 *		jeweiligen Monate als <String> (korrespondierend zu categories[]) 
//		 * 		die summierten Ausgaben als <Number> für die Höhe des Balkens
//		 */
//		
//		String[] categories = {"01", "02", "03"};	
//		int i = (dao.getDistinctShops()).size();	//wie viele verschiedene Läden enthält die SQL Abfrage
//		
//		ArrayList<String> shops = dao.getDistinctShops();	// Methode liefert ein Array mit den Läden
//		ArrayList<Series<String, Number>> allSeries = new ArrayList<>();	
//		
//		CategoryAxis xAxis = new CategoryAxis();	
//		xAxis.setLabel("Monat");
//		
//		NumberAxis yAxis = new NumberAxis();		
//		yAxis.setLabel("Euro");		
//
//		for(int j = 0; j < i; j++) {
//			allSeries.add(dao.getDataSerieForBarChart(shops.get(j)));
//			allSeries.get(j).setName(shops.get(j));
//		}
//		
//		xAxis.setCategories(FXCollections.observableArrayList(categories)); 
//		
//		BarChart<String, Number> barChart = new BarChart<>(xAxis, yAxis); 
//		barChart.setTitle("Die Top-Lebensmittelläden");
//			
//		barChart.getData().addAll(allSeries);	
//		barChart.setPrefWidth(450);
//		
//		//ein kompletter barChart wird zurückgegeben
//		return barChart;
//	}
	
	
}
	
