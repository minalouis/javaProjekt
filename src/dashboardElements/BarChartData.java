package dashboardElements;

public class BarChartData {
    private String shopname;
    private String yearMonth;
    private double sum;

    public String getShopname() {
		return shopname;
	}

	public void setShopname(String shopname) {
		this.shopname = shopname;
	}

	public String getyearMonth() {
		return yearMonth;
	}

	public void setyearMonth(String yearMonth) {
		this.yearMonth = yearMonth;
	}

	public double getSum() {
		return sum;
	}

	public void setSum(double sum) {
		this.sum = sum;
	}

	@Override
    public String toString() { 
        return String.format("[%s,%s,%0.2f]%n", shopname, yearMonth, sum);
    }
}
