package application;

import java.time.LocalDate;
import java.util.HashMap;
import dao.Category;
import dao.Item;
import dao.SqliteItemDAO;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
/**
 * NewEntryStage Stage
 * erstellt einen Fensterinhalt mit Eingabefeldern für die Eintragung neuer Ausgaben
 * in die Datenbank
 * 
 * @author phamann
 * 06.03.2018
 */
public class NewEntryStage extends Stage {
	private final int FOOD_CATEGORY_ID = 5;	//id der Kategorie Lebensmittel
	private String sql;
	
	private TextField entry = new TextField();
	private TextField amount = new TextField();
	private DatePicker when = new DatePicker(LocalDate.now()); 
	private ComboBox<Category> divisions;
	private ComboBox<Category> subdivisions;
	private ComboBox<Category> shops;
	
	private HashMap<String, Label> errorLabel  = new HashMap<>();	
		//nimmt Fehlermeldungen bei falschen/fehlenden Eingaben auf
	private static SqliteItemDAO dao = new SqliteItemDAO();
	
// Konstruktor
	public NewEntryStage() {
		// füge leere Fehler-Label zur HashMap hinzu
		errorLabel.put("Description", new Label());
		errorLabel.put("Amount", new Label());
		errorLabel.put("Category", new Label());
		errorLabel.put("Subcategory", new Label());
		errorLabel.put("Shop", new Label());
		
		Label headlineNewEntry = new Label("Eine neue Ausgabe erfassen");
		Label entryLabel;	
		
		GridPane entryGridPane = new GridPane();  		
	    entryGridPane.setGridLinesVisible(false);	    
	    entryGridPane.getChildren().add(headlineNewEntry);        
        entryGridPane.setPrefWidth(300);
        entryGridPane.setVgap(10);
        entryGridPane.setHgap(10);
        entryGridPane.setPadding(new Insets(10, 10, 10, 10));        

// Formularfelder
        entryLabel = new Label("was?");
        entry.setMaxWidth(200);
        entryGridPane.add(entryLabel, 0, 1);
        entryGridPane.add(entry, 1,1);
        entryGridPane.add(errorLabel.get("Description"), 2,1);
        
        entryLabel = new Label("wieviel?");
        amount.setMaxWidth(200); 
        entryGridPane.add(entryLabel, 0, 2);
        entryGridPane.add(amount, 1,2);
        entryGridPane.add(errorLabel.get("Amount"), 2,2);
        
        entryLabel = new Label("wann?");
        amount.setMaxWidth(200); 	        
        entryGridPane.add(entryLabel, 0, 3);
        entryGridPane.add(when, 1,3);
        
// ------------- ComboBoxes für Auswahl von Kategorie, Unterkategorie, Shop 

        sql = "SELECT id_division, division_name FROM division " +
        		"WHERE id_division != 0 " +
        		"ORDER BY division_name";
        /*
         * die ObservableList nimmt Objekte von Category auf, in denen die ID und der Name
         * einer Kategorie (und auch von Unterkategorien) gespeichert werden
         */
        ObservableList<Category> divisionOptions = dao.getComboBoxItems(sql);
        
        divisions = new ComboBox<>(divisionOptions);  
        divisions.setPrefWidth(200);
        
        subdivisions = new ComboBox<>();
        subdivisions.setPrefWidth(200);
        subdivisions.setDisable(true);
        entryGridPane.add(subdivisions, 1, 5);   
        
        shops = new ComboBox<>();   
        shops.setDisable(true);
        shops.setPrefWidth(200);
           
        //TODO oder mit onselect....
        // Überwachung, welche Kategorie ausgewählt wird:
        divisions.valueProperty().addListener((observable, oldValue, newValue) -> 
        {
        	ObservableList<Category> subdivisionOptions;
        	
     	   	String cat = divisions.getSelectionModel().getSelectedItem().toString();  //ausgewählte Kategorie
     	   	
     	   	// wenn Kategorie gewählt, passende Unterkategorien abrufen und in Liste speichern
     	   	if(newValue != null) {	     	   		
     	   		sql = "SELECT id_subdivision, subdivision_name, division_name FROM subdivision s " +
     	        	  "LEFT JOIN division d " +
     	        	  "ON d.id_division = s.division_id " +
     	        	  "WHERE d.division_name = '" + cat +
     	        	  "' ORDER BY subdivision_name";
     	   		
     	        subdivisionOptions = dao.getComboBoxItems(sql); 
     	        
     	        // Shops zur Auswahl, wenn Kategorie == Lebensmittel      
         	   	if(divisions.getSelectionModel().getSelectedItem().getId() == FOOD_CATEGORY_ID) {
         	   		shops.setDisable(false);
    	            sql = "SELECT id_shop, shop_name FROM shop ORDER BY shop_name";
    	            ObservableList<Category> shopOptions = dao.getComboBoxItems(sql);
    	            shops.setItems(shopOptions);  
         	   	}
         	   	else {
         	   		shops.getItems().clear();
         	   		shops.setDisable(true);
         	   	}         	   		
     	   	} 
     	   	else
     	   		subdivisionOptions = null;
     	   	
     	   	subdivisions.setItems(subdivisionOptions);
     	   	subdivisions.setDisable(false);
     	   	subdivisions.setValue(null);     
        });
        
 // Kategorie     
        entryLabel = new Label("Kategorie");
        divisions.setPromptText("bitte auswählen");
        entryGridPane.add(entryLabel, 0, 4);
        entryGridPane.add(divisions, 1, 4);
        entryGridPane.add(errorLabel.get("Category"), 2,4);	
        	// zeige ErrorLabel zur fehlenden Kategorieauswahl
        
// Unterkategorie        
        entryLabel = new Label("Unterkategorie");
        entryGridPane.add(entryLabel, 0, 5);
        entryGridPane.add(errorLabel.get("Subcategory"), 2, 5);

 // Shops       
        entryLabel = new Label("Laden");        
        entryGridPane.add(entryLabel, 0, 6);
        entryGridPane.add(shops, 1, 6);     
        entryGridPane.add(errorLabel.get("Shop"), 2, 6);
        
// Senden Button		
		Button sendEntry = new Button("eintragen");
		entryGridPane.add(sendEntry, 1, 7);		
		sendEntry.setOnAction(e -> checkItem());  
			//wenn Button geklickt wird, Überprüfung der Eingaben starten

// Beenden Button
		Button quit = new Button("beenden");
		entryGridPane.add(quit, 1, 8);
		quit.setOnAction(clicked -> {
			Main.getPrimaryStage().getScene().setRoot(new Dashboard());
			close();	
		});
			//vor dem Schließen wird Aktualisierung der Diagramme ausgelöst

// Szene erstellen	
		Scene scene = new Scene(entryGridPane, 600, 400);
		this.setScene(scene);
		this.initModality(Modality.WINDOW_MODAL);	
	}
	
/*
 * checkItem() Eingaben prüfen und dann in die Datenbank damit
 * try Block: Versuch, die Item-Member zu setzen, über Methoden, die die 
 */	
	private void checkItem() {
		Item newItem = new Item();		
		
		// foreach Schleife, die HashMap Label-Texte löscht, bei neuen Fehleingaben 
		// setzen die Exceptions neue ErrorLabel-Texte	
		for(String s : errorLabel.keySet()) {
			errorLabel.get(s).setText("");
		}
			
		try {				
			newItem.setDate(when.getValue().toString());
			newItem.setItemDescription(getItemDescriptionFromUserInput());
			newItem.setAmount(getAmountFromUserInput());
			getDivisionIdFromUserInput();
			newItem.setSubdivisionId(getSubdivisionIdFromUserInput());
			newItem.setShop_id(getShopIdFromUserInput());
			
			//neues Item in die Datenbank eintragen
			dao.insertItem(newItem);
		}
		catch (WrongEntryException e) {
			errorLabel.get(e.getLabelKey()).setText(e.getMessage());
		}
	}
	
/*
 * Methoden zur Validierung der Eingaben
 */
	private String getItemDescriptionFromUserInput() {
		String item_description = entry.getText(); 
			if(item_description.trim().isEmpty())
				throw new WrongEntryException("Beschreibung fehlt","Description");
			else
				return item_description;
	}
	
	private double getAmountFromUserInput() {
		String item_amount = amount.getText();
		try {
			return Double.parseDouble(item_amount);	
		}
		catch(NumberFormatException e) {
			throw new WrongEntryException("leer/Buchstaben","Amount");
		}
	} 

	private int getDivisionIdFromUserInput() {
		try {
			divisions.getSelectionModel().getSelectedItem().getId();
			return divisions.getSelectionModel().getSelectedItem().getId();
		}
		catch(NullPointerException e) {
			throw new WrongEntryException("Kategorie wählen","Category");	
		}	
	}

	private int getSubdivisionIdFromUserInput() {
		try {
			return subdivisions.getSelectionModel().getSelectedItem().getId();
		}
		catch(NullPointerException e) {
			throw new WrongEntryException("Unter-Kategorie wählen","Subcategory");	
		}	
	}
	
	private int getShopIdFromUserInput() {
		int shopId = 0;
		try {
			if(getDivisionIdFromUserInput() == FOOD_CATEGORY_ID)
				shopId = shops.getSelectionModel().getSelectedItem().getId();
			return shopId;
		}
		catch(NullPointerException e) {
			throw new WrongEntryException("Laden wählen","Shop");	
		}
	}
}	
