package application;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
/**
 * Navigationsbereich mit Buttons für neuen Eintrag
 * Daten ändern (TODO)
 * Auswertungen anzeigen (TODO)
 * @author phamann
 * 06.03.2018
 */

public class NaviBox extends GridPane {
	private Button newEntryButton;
	private Button changeButton;
	private Button evaluationButton;
	
	public Button getNewEntryButton() {
		return newEntryButton;
	}

	public Button getChangeButton() {
		return changeButton;
	}

	public Button getEvaluationButton() {
		return evaluationButton;
	}

	public NaviBox() {
    	Label headline = new Label("Was soll erledigt werden?");
    	
    	newEntryButton = new Button("Ausgabe eintragen");
    	newEntryButton.setPrefWidth(120);
    	
    	changeButton = new Button("Daten ändern");
    	changeButton.setPrefWidth(120);
    	
    	evaluationButton = new Button("Auswertungen");
    	evaluationButton.setPrefWidth(120);
      
        this.setGridLinesVisible(false);        
        this.setPrefWidth(220);
        this.setVgap(10);
        this.setHgap(10);        
        this.add(headline, 0, 0, 2, 1);        
        this.add(newEntryButton, 0, 2);
        this.add(changeButton, 0, 3);
        this.add(evaluationButton, 0, 4);
        this.setMaxWidth(150);			
	}
}
