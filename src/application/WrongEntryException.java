package application;

@SuppressWarnings("serial")
public class WrongEntryException extends IllegalArgumentException {
	
	private String labelKey;
	
	public WrongEntryException(String message, String labelKey) {
		super(message);
		this.labelKey = labelKey;		
	}
	
	public String getLabelKey() {
		return labelKey;
	}
}
