package application;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.image.Image;

/**
 * Startseite der Anwendung Haushaltsbuch
 * @author phamann
 * 06.03.2018
 */

public class Main extends Application {  

	private static Stage primaryStage;
	
	public static Stage getPrimaryStage() {
		return Main.primaryStage;
	} 
	
	@Override
	public void start(Stage primaryStage) {	
		Main.primaryStage = primaryStage;
		
// ---------------- Seitenaufbau ------------------------------------ //       
		// ausgelagert in Dashboard.java 
		
// -------------- Szene erstellen ------------------------------------ //       
		Scene scene = new Scene(new Dashboard(), 900, 650);
	
// -------------- Szene auf die Bühne schieben ----------------------- //    
		primaryStage.setScene(scene);
		primaryStage.setMinWidth(600);
		primaryStage.setTitle("Haushaltsbuch");
		primaryStage.getIcons().add(new Image("graphics/euro.png"));
		
// -------------------- Vorhang auf ---------------------------------- //
		primaryStage.show();	
    }

    public static void main(String[] args) {
        launch(args);
    }
}
