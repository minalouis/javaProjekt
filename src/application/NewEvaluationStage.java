package application;

import dao.Category;
import dao.SqliteItemDAO;
import dashboardElements.NonChartsElements;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class NewEvaluationStage extends Stage{
	private Text headline = new Text();
	private ComboBox<Category> divisions;
	private String sql;
	private static SqliteItemDAO dao = new SqliteItemDAO();
	
	public NewEvaluationStage() {
		GridPane evaluationPane = new GridPane();  		
	    evaluationPane.setGridLinesVisible(false);	    
        evaluationPane.setVgap(10);
        evaluationPane.setHgap(10);
        evaluationPane.setPadding(new Insets(15, 10, 15, 10));   
        
	    evaluationPane.getChildren().add(getHeadline());  	    
	    evaluationPane.add(new Label("2018"),0,1);
	    
	    evaluationPane.add(new Label("Einkommen ges."),1,1);
	    evaluationPane.add(new Label(String.format("%.2f €", getIncome())),2,1);
	    
	    evaluationPane.add(new Label("Ausgaben ges."),1,2);
	    evaluationPane.add(new Label(String.format("%.2f €", getSpendings())),2,2);
	    
	    double totalResult = getDifference(getIncome(), getSpendings());
	    //Test, ob Minus rot dargestellt wird
	    //totalResult = -100.0;
	   	evaluationPane.add(new Label("Ergebnis"),1,3); 	   	
	   	Label yearResult = new Label(String.format("%.2f €", totalResult));	   	
	   	evaluationPane.add(yearResult, 2, 3);
	   	if(totalResult < 0)
	   		yearResult.setTextFill(Color.RED);
	   	
	   	//ComboBox für die Auswahl der Auswertungskategorie
	   	sql = "SELECT id_division, division_name FROM division " +
        		"WHERE id_division != 0 " +
        		"ORDER BY division_name";
        /*
         * die ObservableList nimmt Objekte von Category auf, in denen die ID und 
         * der Name einer Kategorie gespeichert werden
         */
        ObservableList<Category> divisionOptions = dao.getComboBoxItems(sql);        
        divisions = new ComboBox<>(divisionOptions);  
        divisions.setPrefWidth(200);
        Label evalLabel = new Label("Auswertung nach Kategorie");
        divisions.setPromptText("bitte auswählen");
        evaluationPane.add(evalLabel, 0, 7);
        evaluationPane.add(divisions, 1, 7);
        
        //TODO weg damit, wenns nicht geht....
        divisions.setOnAction(e -> 
        	evaluationPane.add(NonChartsElements.getTableViewEvaluation(divisions.getSelectionModel().getSelectedItem() ), 0, 8, 3, 1));
        
        //Auswertungstabelle einfügen
	   //	evaluationPane.add(NonChartsElements.getTableViewEvaluation(), 0, 8, 3, 1);
	   	
	   	//Beenden Button einfügen
		Button quit = new Button("beenden");
		evaluationPane.add(quit, 0, 10);
		quit.setOnAction(clicked -> close());
	   	
	   	
 // Szene erstellen	
 		Scene scene = new Scene(evaluationPane, 800, 600);
 		this.setScene(scene);
 		this.initModality(Modality.WINDOW_MODAL);	
 		
 		this.setTitle("Auswertungen");
 		this.getIcons().add(new Image("graphics/euro.png"));
 	}

	//Inhalte generieren	
	private Text getHeadline() {
		headline.setText("Auswertungen");
		headline.setFill(Color.DARKGREEN );
	    headline.setFont(Font.font(16));
	    
		return headline;
	}
	
	private double getIncome() {
		double result;
		sql = "SELECT SUM(amount) AS Einkommen FROM item" +
			  " WHERE income_id != 0 AND strftime('%Y', date) = strftime('%Y', 'now')";
		result = dao.getSingleResult(sql);
		
		return result;
	}
	
	private double getSpendings() {
		double result;
		sql = "SELECT SUM(amount) AS Ausgaben FROM item" + 
			  " WHERE income_id = 0 AND strftime('%Y', date) = strftime('%Y', 'now')";
		result = dao.getSingleResult(sql);
		
		return result;
	}
	private double getDifference(double minuend, double subtrahend) {
		double result = 0.0;
		result = minuend - subtrahend;
	
		return result;
	}
}
