package application;

import dashboardElements.ChartsForDashboard;
import dashboardElements.NonChartsElements;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Dashboard - Inhalt der Startseite
 * 
 * @author phamann
 * 06.03.2018
 */
public class Dashboard extends VBox {
	/*
	 * Dashboard ist eine VBox
	 * Dashboard hat zwei HBoxes
	 * zu den HBoxes werden Charts, eine NaviBox (NaviBox.java), ein Kalender und ein
	 * TableView hinzugefügt
	 */
	private	HBox hbox1 = new HBox();  
	private	HBox hbox2 = new HBox();
	private	NaviBox naviBox = new NaviBox();     
	
	/*
	 * Methode newEntryStage 
	 * erzeugt ein Objekt von NewEntryStage, welches angezeigt wird, wenn der Button
	 * "Ausgabe eintragen" aus der NaviBox angeklickt wird
	 * Fenster gehört zur primaryStage (owner)
	 * 
	 */	
	void newEntryStage(Stage primaryStage) {
		NewEntryStage newEntryWindow = new NewEntryStage();
		newEntryWindow.initOwner(primaryStage);
		newEntryWindow.show();
	}
	
	void newEvaluationStage(Stage primaryStage) {
		NewEvaluationStage newEntryWindow = new NewEvaluationStage();
		newEntryWindow.initOwner(primaryStage);
		newEntryWindow.show();
	}
	
// Konstruktor 
	public Dashboard(){
        this.setPadding(new Insets(10)); 
        this.setSpacing(50);
        
        hbox1.setSpacing(10);
        hbox1.setAlignment(Pos.CENTER);
        hbox1.setMaxHeight(300);  
        
        hbox1.getChildren().add(naviBox);
        hbox1.getChildren().add(NonChartsElements.getPopUpContent()); 	
        hbox1.getChildren().add(ChartsForDashboard.getBarChartDashboard());
        
        hbox2.setSpacing(10);
        hbox2.setAlignment(Pos.CENTER);
        
        hbox2.getChildren().add(ChartsForDashboard.getPieChartDashboard());
        hbox2.getChildren().add(NonChartsElements.getTableView());
        
        naviBox.getNewEntryButton().setOnAction(e->newEntryStage(Main.getPrimaryStage()));
        naviBox.getEvaluationButton().setOnAction(e->newEvaluationStage(Main.getPrimaryStage()));
        
        this.getChildren().add(hbox1);
        this.getChildren().add(hbox2);
	}

}
