--
-- File generated with SQLiteStudio v3.1.1 on Di Mrz 6 10:02:02 2018
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: deposit
DROP TABLE IF EXISTS deposit;

CREATE TABLE deposit (
    deposit_name   TEXT NOT NULL,
    deposit_amount REAL NOT NULL,
    actual_stock   REAL
);

INSERT INTO deposit (
                        deposit_name,
                        deposit_amount,
                        actual_stock
                    )
                    VALUES (
                        'childrens_spendings',
                        100,
                        1200
                    );


-- Table: division
DROP TABLE IF EXISTS division;

CREATE TABLE division (
    id_division   INTEGER PRIMARY KEY
                          UNIQUE
                          NOT NULL,
    division_name TEXT    NOT NULL
);

INSERT INTO division (
                         id_division,
                         division_name
                     )
                     VALUES (
                         0,
                         'Sonstiges'
                     );

INSERT INTO division (
                         id_division,
                         division_name
                     )
                     VALUES (
                         1,
                         'Persönliches'
                     );

INSERT INTO division (
                         id_division,
                         division_name
                     )
                     VALUES (
                         2,
                         'Versicherungen'
                     );

INSERT INTO division (
                         id_division,
                         division_name
                     )
                     VALUES (
                         3,
                         'Kinder'
                     );

INSERT INTO division (
                         id_division,
                         division_name
                     )
                     VALUES (
                         4,
                         'Wohnen'
                     );

INSERT INTO division (
                         id_division,
                         division_name
                     )
                     VALUES (
                         5,
                         'Lebensmittel'
                     );


-- Table: fixed_amount
DROP TABLE IF EXISTS fixed_amount;

CREATE TABLE fixed_amount (
    subdivision_name TEXT    NOT NULL,
    fixed_amount     REAL    NOT NULL,
    months           INTEGER
);

INSERT INTO fixed_amount (
                             subdivision_name,
                             fixed_amount,
                             months
                         )
                         VALUES (
                             'hausratversicherung',
                             200,
                             12
                         );

INSERT INTO fixed_amount (
                             subdivision_name,
                             fixed_amount,
                             months
                         )
                         VALUES (
                             'chor',
                             30,
                             3
                         );

INSERT INTO fixed_amount (
                             subdivision_name,
                             fixed_amount,
                             months
                         )
                         VALUES (
                             'strom',
                             80,
                             1
                         );


-- Table: income
DROP TABLE IF EXISTS income;

CREATE TABLE income (
    id_income   INTEGER PRIMARY KEY
                        NOT NULL
                        UNIQUE,
    income_kind TEXT    NOT NULL
);

INSERT INTO income (
                       id_income,
                       income_kind
                   )
                   VALUES (
                       0,
                       'sonstiges Einkommen'
                   );

INSERT INTO income (
                       id_income,
                       income_kind
                   )
                   VALUES (
                       1,
                       'Kindergeld'
                   );

INSERT INTO income (
                       id_income,
                       income_kind
                   )
                   VALUES (
                       2,
                       'Gehalt'
                   );


-- Table: item
DROP TABLE IF EXISTS item;

CREATE TABLE item (
    id_item          INTEGER PRIMARY KEY AUTOINCREMENT
                             NOT NULL
                             UNIQUE,
    item_description TEXT    NOT NULL,
    date             DATE    NOT NULL,
    amount           DOUBLE  NOT NULL,
    subdivision_id   INTEGER REFERENCES subdivision,
    shop             TEXT,
    shop_id          INTEGER REFERENCES shop (id_shop),
    income_id        INTEGER REFERENCES income (id_income) ON UPDATE NO ACTION
);

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     1,
                     'Gehalt',
                     '2018-01-01',
                     2000,
                     0,
                     '0',
                     0,
                     2
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     2,
                     'Kindergeld',
                     '2018-01-01',
                     384,
                     0,
                     '0',
                     0,
                     1
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     3,
                     'Miete',
                     '2018-01-01',
                     900,
                     31,
                     '0',
                     0,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     4,
                     'Strom',
                     '2018-01-02',
                     80,
                     32,
                     '0',
                     0,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     5,
                     'Lebensmittel',
                     '2018-01-03',
                     89.85,
                     38,
                     '0',
                     1,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     6,
                     'Jeans Elijah',
                     '2018-01-03',
                     20,
                     20,
                     '0',
                     0,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     7,
                     'Sportschuhe Helen',
                     '2018-01-05',
                     39.95,
                     20,
                     '0',
                     0,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     8,
                     'Materialgeld Helen',
                     '2018-01-05',
                     45,
                     19,
                     '0',
                     0,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     9,
                     'Musikschule Elijah',
                     '2018-01-08',
                     75.54,
                     21,
                     '0',
                     0,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     10,
                     'Chor',
                     '2018-01-15',
                     30,
                     10,
                     '0',
                     0,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     11,
                     'Pullover',
                     '2018-01-25',
                     39,
                     11,
                     '0',
                     0,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     12,
                     'Kunsthalle',
                     '2018-01-20',
                     15,
                     4,
                     '0',
                     0,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     13,
                     'Kino Helen',
                     '2018-01-22',
                     8,
                     24,
                     '0',
                     0,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     14,
                     'Lebensmittel',
                     '2018-01-16',
                     65.45,
                     38,
                     '0',
                     3,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     15,
                     'Lebensmittel',
                     '2018-01-22',
                     12.45,
                     38,
                     '0',
                     4,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     16,
                     'Geschenk Ina',
                     '2018-01-25',
                     12.99,
                     1,
                     '0',
                     0,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     18,
                     'Allianz Rente',
                     '2018-01-18',
                     110.25,
                     14,
                     '0',
                     0,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     19,
                     'Telekom',
                     '2018-01-25',
                     60,
                     36,
                     '0',
                     0,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     20,
                     'BSAG',
                     '2018-01-29',
                     10.45,
                     6,
                     '0',
                     0,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     21,
                     'Wein',
                     '2018-01-26',
                     19.5,
                     40,
                     '0',
                     0,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     22,
                     'Lebensmittel',
                     '2018-01-30',
                     31.88,
                     38,
                     '0',
                     4,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     23,
                     'Notenheft',
                     '2018-01-17',
                     2.99,
                     12,
                     '0',
                     0,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     24,
                     'Brötchen',
                     '2018-01-19',
                     4.85,
                     38,
                     '0',
                     7,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     25,
                     'Blumen',
                     '2018-01-31',
                     20,
                     28,
                     '0',
                     4,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     26,
                     'Glühwein',
                     '2018-02-05',
                     10.99,
                     40,
                     '0',
                     2,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     27,
                     'Hot Dogs',
                     '2018-02-25',
                     6,
                     40,
                     'IKEA',
                     0,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     28,
                     'Rock Helen',
                     '2018-02-19',
                     9.99,
                     20,
                     'H&M',
                     0,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     29,
                     'Lebensmittel',
                     '2018-02-25',
                     12.99,
                     38,
                     '0',
                     3,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     31,
                     'Weihnachtsessen',
                     '2017-12-19',
                     65.87,
                     38,
                     'Rewe',
                     1,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     32,
                     'Brötchen',
                     '2017-12-21',
                     4.95,
                     38,
                     '',
                     7,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     33,
                     'Lebensmittel',
                     '2018-03-01',
                     25.56,
                     38,
                     NULL,
                     1,
                     NULL
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     34,
                     'Lebensmittel',
                     '2018-02-05',
                     89.69,
                     38,
                     NULL,
                     5,
                     NULL
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     35,
                     'Lebensmittel',
                     '2018-02-10',
                     45.78,
                     38,
                     NULL,
                     1,
                     NULL
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     36,
                     'Kopierpapier',
                     '2018-03-05',
                     4,
                     5,
                     NULL,
                     0,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     37,
                     'Kuchen',
                     '2018-03-05',
                     5,
                     38,
                     NULL,
                     2,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     38,
                     'Markt Richter',
                     '2018-03-05',
                     34,
                     38,
                     NULL,
                     4,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     39,
                     'Lebensmittel',
                     '2018-03-05',
                     12,
                     38,
                     NULL,
                     1,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     40,
                     'Geburtstagbrunch',
                     '2018-03-05',
                     100,
                     38,
                     NULL,
                     1,
                     0
                 );

INSERT INTO item (
                     id_item,
                     item_description,
                     date,
                     amount,
                     subdivision_id,
                     shop,
                     shop_id,
                     income_id
                 )
                 VALUES (
                     41,
                     'Lebensmittel',
                     '2018-03-06',
                     200,
                     38,
                     NULL,
                     3,
                     0
                 );


-- Table: shop
DROP TABLE IF EXISTS shop;

CREATE TABLE shop (
    id_shop   INTEGER PRIMARY KEY
                      UNIQUE
                      NOT NULL,
    shop_name TEXT    NOT NULL
);

INSERT INTO shop (
                     id_shop,
                     shop_name
                 )
                 VALUES (
                     0,
                     'sonstiger Shop'
                 );

INSERT INTO shop (
                     id_shop,
                     shop_name
                 )
                 VALUES (
                     1,
                     'Rewe'
                 );

INSERT INTO shop (
                     id_shop,
                     shop_name
                 )
                 VALUES (
                     2,
                     'Aleco'
                 );

INSERT INTO shop (
                     id_shop,
                     shop_name
                 )
                 VALUES (
                     3,
                     'Netto'
                 );

INSERT INTO shop (
                     id_shop,
                     shop_name
                 )
                 VALUES (
                     4,
                     'Markt'
                 );

INSERT INTO shop (
                     id_shop,
                     shop_name
                 )
                 VALUES (
                     5,
                     'Penny'
                 );

INSERT INTO shop (
                     id_shop,
                     shop_name
                 )
                 VALUES (
                     6,
                     'Edeka'
                 );

INSERT INTO shop (
                     id_shop,
                     shop_name
                 )
                 VALUES (
                     7,
                     'Bäcker'
                 );

INSERT INTO shop (
                     id_shop,
                     shop_name
                 )
                 VALUES (
                     8,
                     'Alnatura'
                 );

INSERT INTO shop (
                     id_shop,
                     shop_name
                 )
                 VALUES (
                     9,
                     'Lidl'
                 );

INSERT INTO shop (
                     id_shop,
                     shop_name
                 )
                 VALUES (
                     10,
                     'Aldi'
                 );


-- Table: subdivision
DROP TABLE IF EXISTS subdivision;

CREATE TABLE subdivision (
    id_subdivision   INTEGER PRIMARY KEY
                             UNIQUE
                             NOT NULL,
    division_id      INTEGER REFERENCES division (id_division),
    subdivision_name TEXT    NOT NULL
);

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            0,
                            0,
                            'sonstige Unterkategorie'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            1,
                            1,
                            'Geschenke'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            2,
                            1,
                            'Kosmetik'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            3,
                            1,
                            'Gesundheit'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            4,
                            1,
                            'Eintritt'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            5,
                            1,
                            'Büromaterial'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            6,
                            1,
                            'Mobilität'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            7,
                            1,
                            'Bücher und Medien'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            8,
                            1,
                            'Porto und Gebühren'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            9,
                            1,
                            'Sonstiges'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            10,
                            1,
                            'Chor'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            11,
                            1,
                            'Kleidung'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            12,
                            2,
                            'Zahnzusatzversicherung'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            13,
                            2,
                            'Brillenversicherung'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            14,
                            2,
                            'Allianz Rente'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            15,
                            2,
                            'Unfallversicherung'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            16,
                            2,
                            'Lebensversicherung'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            17,
                            2,
                            'Bausparen'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            18,
                            2,
                            'AuslandsKV'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            19,
                            3,
                            'Schule'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            20,
                            3,
                            'Bekleidung'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            21,
                            3,
                            'Musikunterricht'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            22,
                            3,
                            'Geschenke'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            23,
                            3,
                            'Betreuung'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            24,
                            3,
                            'Freizeit'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            25,
                            3,
                            'Sonstiges'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            26,
                            3,
                            'Sport'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            27,
                            4,
                            'Verbrauchsmaterial'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            28,
                            4,
                            'Möbel und Deko'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            29,
                            4,
                            'Reparaturen und Wartung'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            30,
                            4,
                            'Sonstiges'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            31,
                            4,
                            'Miete'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            32,
                            4,
                            'Strom'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            33,
                            4,
                            'Gas'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            34,
                            4,
                            'Wasser'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            35,
                            4,
                            'Mobiltelefon'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            36,
                            4,
                            'Telefon und Internet'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            37,
                            4,
                            'GEZ'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            38,
                            5,
                            'Lebensmittel'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            39,
                            5,
                            'Essen auswärts'
                        );

INSERT INTO subdivision (
                            id_subdivision,
                            division_id,
                            subdivision_name
                        )
                        VALUES (
                            40,
                            5,
                            'Genussmittel'
                        );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
