SELECT  s.shop_name,
        SUM(i.amount) AS Summe
    FROM item i
        LEFT JOIN 
        shop s ON i.shop_id = s.id_shop
    WHERE i.shop_id != 0 
    GROUP BY s.shop_name

    ORDER BY Summe DESC
    LIMIT 3;
    
