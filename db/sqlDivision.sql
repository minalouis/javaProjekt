--Divisions
SELECT division_name FROM division
WHERE id_division != 0
ORDER BY division_name;

-- Divisions und deren Summen gesamt
SELECT d.division_name ,SUM(i.amount) 
  FROM item i
       LEFT JOIN
       subdivision sd ON i.subdivision_id = sd.id_subdivision
       LEFT JOIN
       division d ON d.id_division = sd.division_id
 WHERE i.income_id = 0
 GROUP BY d.id_division;
 
--Divisions und deren Summe, aktuelles Jahr
SELECT d.division_name ,SUM(i.amount) 
  FROM item i
       LEFT JOIN
       subdivision sd ON i.subdivision_id = sd.id_subdivision
       LEFT JOIN
       division d ON d.id_division = sd.division_id
 WHERE i.income_id = 0 AND strftime('%Y','now') = strftime('%Y',date)
 GROUP BY d.id_division;
